//
//  ViewController.swift
//  ARkaihatu
//
//  Created by anagai on 2020/04/17.
//  Copyright © 2020 nagainoayumu. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation

class ViewController: UIViewController, ARSCNViewDelegate ,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate{
    
    
    @IBOutlet var sceneView: ARSCNView!
    
    var player :AVPlayer!
    var selectedItem : String? = "sitting"
    var avPlayerItem: AVPlayerItem!
    var playerItem:AVPlayerItem?
//    var videoURL:URL!
    
    
      
//    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        videoURL = (info["UIImagePickerControllerReferenceURL"] as! URL)
//        print(videoURL as Any)
//    }
      
    
    @IBAction func videotap(_ sender: Any) {
        print("UIBarButtonItem。カメラロールから動画を選択")
        let view1 = UIImagePickerController() //アルバムを開く処理を呼び出す
        view1.sourceType = .photoLibrary
        view1.delegate = self
        view1.mediaTypes = ["public.movie"]
        view1.allowsEditing = true
        view1.videoQuality = UIImagePickerController.QualityType.typeHigh
        present(view1, animated: true)
        self.present(view1, animated: true, completion: nil)
    }
    


   
        
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        guard let videoURL = info[.mediaURL] as? URL else { return }
//        self.addVideo(videoURL)
//    }
    
    
    
//    func initItem(item:AVPlayerItem)
//    {
//        playerItem = AVPlayerItem.init(url: (item.asset.value(forKey: "URL") as! NSURL) as URL)
//        player = AVPlayer(playerItem:playerItem)
//        playerController = ViewController()
//        playerController?.player = player
//    }
//

//    func addARTV(){
////        //  ここからビデオのコード
//        let videoURL = Bundle.main.url(forResource: "videoIRON", withExtension: "mp4")!
//        player = AVPlayer(url: videoURL)
//
//
//        let tvGeo = SCNPlane(width: 1.6, height: 0.9)
//        tvGeo.firstMaterial?.diffuse.contents = player
//        tvGeo.firstMaterial?.isDoubleSided = true
//
//        let tvNode = SCNNode(geometry: tvGeo)
//        tvNode.position.z = -5
//
//        sceneView.scene.rootNode.addChildNode(tvNode)
//
//        let configuration = ARWorldTrackingConfiguration()
//        configuration.frameSemantics = .personSegmentationWithDepth
//        sceneView.session.run(configuration)
//
//        tvNode.name = selectedItem
//
//        player.play()
//
//    }
    
    
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        let videoURL = Bundle.main.url(forResource: "videoIRON", withExtension: "mp4")!
//        player = AVPlayer(url: videoURL)
//
//        let tvGeo = SCNPlane(width: 1.6, height: 0.9)
//        tvGeo.firstMaterial?.diffuse.contents = player
//        tvGeo.firstMaterial?.isDoubleSided = true
//
//
//        let tvNode = SCNNode(geometry: tvGeo)
//         tvNode.position.z = -3
//
//
//        let position = SCNVector3(x: 0, y: 0, z: -5)
//        if let camera = sceneView.pointOfView {
//              tvNode.position = camera.convertPosition(position, to: nil)// カメラ位置からの偏差で求めた位置
//              tvNode.eulerAngles = camera.eulerAngles  // カメラのオイラー角と同じにする
//        }
//
//
//        sceneView.scene.rootNode.addChildNode(tvNode)
//
//        player.play()
//    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.delegate = self
        sceneView.showsStatistics = false
        sceneView.debugOptions = ARSCNDebugOptions.showFeaturePoints
        
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // シーンを生成
        let scene = SCNScene()
        
        // Set the scene to the view
        sceneView.scene = scene

        
        let gesture = UITapGestureRecognizer(target: self, action:#selector(onTap))
        self.sceneView.addGestureRecognizer(gesture)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressView))
        sceneView.addGestureRecognizer(longPressGesture)
        
    


        // 表示するテキストを用意
        let str = "please push the button"
        let depth:CGFloat = 0.2 // 奥行き0.2m
        let text = SCNText(string: str, extrusionDepth: depth)
        text.font = UIFont.boldSystemFont(ofSize: 1.0)

        // テキストの色と質感を用意
        // SCNText には最大5つの要素があり、それぞれに SCNMaterial を指定できる
        // front, back, extruded sides, front chamfer, back chamfer
        // front material
        let m1 = SCNMaterial()
        m1.diffuse.contents = UIColor.red // 前面に赤色
        // 鏡面反射感を出す
        m1.lightingModel = .physicallyBased
        m1.metalness.contents = 1.0
        m1.metalness.intensity = 1.0
        m1.roughness.intensity = 0.0
        // back material
        let m2 = SCNMaterial()
        m2.diffuse.contents = UIColor.green // 背面に緑色
        m2.lightingModel = .physicallyBased
        m2.metalness.contents = 1.0
        m2.metalness.intensity = 1.0
        m2.roughness.intensity = 0.0
        // extruded sides material
        let m3 = SCNMaterial()
        m3.diffuse.contents = UIColor.blue // 側面に青色
        m3.lightingModel = .physicallyBased
        m3.metalness.contents = 1.0
        m3.metalness.intensity = 1.0
        m3.roughness.intensity = 0.0
        // front chamfer material
        let m4 = SCNMaterial()
        m4.diffuse.contents = UIColor.yellow
        // back chamfer material
        let m5 = SCNMaterial()
        m5.diffuse.contents = UIColor.yellow

        // テキストの色と質感をセット
        text.materials = [m1, m2, m3, m4, m5]

        // AR空間の要素としてテキストをセット
        let textNode = SCNNode(geometry: text)
        textNode.name = "text"

        // テキストを配置する場所を決める
        let (min, max) = (textNode.boundingBox)
        let textBoundsWidth = (max.x - min.x)
        let textBoundsheight = (max.y - min.y)
        let z = CGFloat(-5.0) // 目の前nメートルの距離
        textNode.pivot = SCNMatrix4MakeTranslation(textBoundsWidth/2 + min.x, textBoundsheight/2 + min.y, 0)
        textNode.position = SCNVector3(0, 0, z)

        // AR空間にテキスト要素を配置
        sceneView.scene.rootNode.addChildNode(textNode)

        // 必要に応じて自動的に光源が追加されるように設定
        sceneView.autoenablesDefaultLighting = true

        //オブジェクトの手前に体を常に表示できる機能(A12のみ)
        let configuration = ARWorldTrackingConfiguration()
        configuration.frameSemantics = .personSegmentation
        sceneView.session.run(configuration)
        

        
    
        // デバッグ用設定
        // バウンディングボックス、ワイヤーフレームを表示する
        //sceneView.debugOptions = [.showBoundingBoxes, .showWireframe]
        
        
        // Create a new scene(飛行機のやつ)
        //        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        //
        //        // Set the scene to the view
        //        sceneView.scene = scene
        
        
        // ビデオのURL
        //        let videoUrl = Bundle.main.url(forResource: "videoIRON", withExtension: "mp4")!
        //        // ビデオ動画をテクスチャーとして適用した縦3mのキューブの生成
        //        let videoNode = createVideoNode(size: 2.0, videoUrl: videoUrl)
        //        // 位置は、カメラの前方5m
        //        videoNode.position = SCNVector3(0, 0, -7.0)
        //        // シーンに追加する
        //        sceneView.scene.rootNode.addChildNode(videoNode)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        
        // 環境マッピングを有効にする
        configuration.environmentTexturing = .automatic
        
        sceneView.session.run(configuration)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        sceneView.session.pause()
    }
    

    
//Arkit 1の古い手法
    @objc func onTap(sender: UITapGestureRecognizer) {
        
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            if let node = node as? ViewController {
            } else {
                node.removeFromParentNode()
            }
        }
        
        let videoURL = Bundle.main.url(forResource: "videoIRON", withExtension: "mp4")!
        player = AVPlayer(url: videoURL)

        
        
        let tvGeo = SCNPlane(width: 1.6, height: 0.9)
        tvGeo.firstMaterial?.diffuse.contents = player
        tvGeo.firstMaterial?.isDoubleSided = true

        let tvNode = SCNNode(geometry: tvGeo)

        let pos = sender.location(in: sceneView)
        tvNode.name = "video"

        let results = sceneView.hitTest(pos, types: .featurePoint)
        if !results.isEmpty {
            let hitTestResult = results.first!
            let transform = hitTestResult.worldTransform
            tvNode.position = SCNVector3 (
                transform.columns.3.x,
                transform.columns.3.y,
                transform.columns.3.z
            )
        
            sceneView.scene.rootNode.addChildNode(tvNode)
        }

        let configuration = ARWorldTrackingConfiguration()
        configuration.frameSemantics = .personSegmentationWithDepth
        sceneView.session.run(configuration)

        player.play()
    }
    
    
    @objc func longPressView(sender: UILongPressGestureRecognizer) {
        if sender.state == .began {
        
            let location = sender.location(in: sceneView)
            let hitTest  = sceneView.hitTest(location)
            if let result = hitTest.first  {
                if result.node.name == "video"
                {
                    player.pause()
                    result.node.removeFromParentNode();
                    
                }
            }
        }
    }
    

    
 
    // MARK: - ARSCNViewDelegate
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
    
    //    func session(_ session: ARSession, didFailWithError error: Error) {
    //        // Present an error message to the user
    //
    //    }
    //
    //
    //
    //    func sessionWasInterrupted(_ session: ARSession) {
    //        // Inform the user that the session has been interrupted, for example, by presenting an overlay
    //
    //    }
    //
    //    func sessionInterruptionEnded(_ session: ARSession) {
    //        // Reset tracking and/or remove existing anchors if consistent tracking is required
    //
    //    }
    
    
    //    func createVideoNode(size:CGFloat, videoUrl: URL) -> SCNNode {
    //        // AVPlayerを生成する
    //        let avPlayer = AVPlayer(url: videoUrl)
    //
    //        // SKSceneを生成する
    //        let skScene = SKScene(size: CGSize(width: 1000, height: 1000)) // あまりサイズが小さいと、ビデオの解像度が落ちる
    //
    //        // AVPlayerからSKVideoNodeの生成する
    //        let skNode = SKVideoNode(avPlayer: avPlayer)
    //        // シーンと同じサイズとし、中央に配置する
    //        skNode.position = CGPoint(x: skScene.size.width / 2.0, y: skScene.size.height / 2.0)
    //        skNode.size = skScene.size
    //        skNode.yScale = -1.0 // 座標系を上下逆にする
    //        skNode.play() // 再生開始
    //
    //        // SKSceneに、SKVideoNodeを追加する
    //        skScene.addChild(skNode)
    //
    //        // SCNBoxノードを生成して、マテリアルにSKSeceを適用する
    //        let node = SCNNode()
    //        node.geometry = SCNBox(width: size, height: size, length: size, chamferRadius: 0)
    //        let material = SCNMaterial()
    //        material.diffuse.contents = skScene
    //        node.geometry?.materials = [material]
    //        node.scale = SCNVector3(1.7, 1, 1) // サイズは横長
    //        return node
    //    }
    
    
    

}
